# Apache Pig

1. inherits from `java:7-jre`
2. downloads Apache Pig [(see)](http://pig.apache.org/docs/r0.15.0/start.html#download)
3. adds Pig to `PATH`
4. runs pig in local mode

## How to use

1. running Grunt console
	```
	docker run --rm -it tinchou/apache-pig
	```
2. running a script in the current folder
	```
	docker run --rm -it -v /your/host/dir/with/code:/code:rw tinchou/apache-pig your_script.pig
	```

## Thanks

Thanks to Martin Chalupa for creating the [original image](https://hub.docker.com/r/chalimartines/local-pig/) I used for reference.
