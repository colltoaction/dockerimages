# Usage

- Mount the folder with your code in the `/code` directory
- Specify the filename or filenames you want to run

```
$ docker run -v "$PWD":/code tinchou/envr script.R
```
